const app = Vue.createApp({
    data(){
        return {
            phoneNumber: 123456789
        }
    },
    methods: {
        update(){
            if(this.phoneNumber == 123456789){
                this.phoneNumber = 987654321;
            }else{
                this.phoneNumber = 123456789;
            }
        },
        setCustomNumber(event){
            this.phoneNumber = event.target.value;
        }
    },
    computed: {
        backwardsNumber(){
            var numString = this.phoneNumber.toString();
            numString = numString.split("");
            numString.reverse();
            numString = numString.join("");
            return numString;
        }
    }
});

window.onload = function(){
    app.mount("#main");
};